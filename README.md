This is part of the project FRETIS. This project is a complete desktop application which helps the connection between the AEON SDK Realtime Communications with the Web Service
provided by TREDIT S.A. which further processes the data. 
This application receives and sends realtime data about Warehouses.
It reads the data either in XML or CSV format and provides a Graphic User Interface where several actions can take place as well as realtime logs are provided about the current
state of the application, the data that are exchanged as well as errors or warnings that may occur. 

To run this application locally you should ask for the credentials needed in Config.java.

For further information about this project: 

http://www.tredit.gr/site/wp-content/uploads/2014/11/FREight-Transport-Information-Technology-Solutions-FRETIS-GR-Overview.pdf