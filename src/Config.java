import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
/**
 * This class initializes the variables given in config.txt which are going to determine the
 * behaviour of the program.
 * Also, the credentials which are used for authentication for AEON SDK for Realtime Communications are initialized.
 *
 */
public class Config {
    /*
	 * Config for subscriptions. If you get
	 * errors about subscription in use try
	 * with other id and desc values.
	 *
	 * These values represents your process
	 * in the AEON network and needs to be
	 * unique.
	 *
	 */

    private static String subUrl,pubUrl,id,desc,dataType,fetchMethod;

    public static final String SUB_URL =  "https://aeon.atosresearch.eu:3000/subscribe/*****-****-****-******";
    public static final String _ID = "*******";
    public static final String _DESC = "Warehouse";
    public static final String PUB_URL = "https://aeon.atosresearch.eu:3000/publish/*****-****-****-****-*****";


    //reads the config.txt file which should be located inside the server, and initializes the variables.The explanation of each variable,
    //as well as their use are described on README.txt file.

    public void initializeVariables(){
        BufferedReader br =null;
        boolean flag=true;

        int filePos=0;
        try{
            br = new BufferedReader(new FileReader("config.txt"));
            String line="";
            while(flag){
                if(filePos==0){
                    line=br.readLine();
                    String[] lineElements = line.split("::");
                    pubUrl=lineElements[1];
                    filePos++;
                }
                else if(filePos==1){
                    line=br.readLine();
                    String[] lineElements = line.split("::");
                    if(lineElements.length>1) {
                        subUrl = lineElements[1];
                    }
                    filePos++;
                }
                else if(filePos==2){
                    line=br.readLine();
                    String[] lineElements = line.split("::");
                    id = lineElements[1];
                    filePos++;
                }
                else if(filePos==3){
                    line=br.readLine();
                    String[] lineElements = line.split("::");
                    if(lineElements.length>1) {
                        desc = lineElements[1];
                    }
                    filePos++;
                }
                else if(filePos==4){
                    line=br.readLine();
                    String[] lineElements=line.split("::");
                    if(lineElements.length>1) {
                        dataType = lineElements[1];
                    }
                    filePos++;
                }
                else if(filePos==5){
                    line=br.readLine();
                    String[] lineElements=line.split("::");
                    if(lineElements.length>1) {
                        fetchMethod = lineElements[1];
                    }
                    filePos++;
                }
                else break;


            }
        }catch (IOException e){
            e.printStackTrace();
        }

        System.out.println("publish: " + pubUrl + " \nsubscribe: " + subUrl + " \nid: " + id +
                " \ndesc: " + desc + " \nfetch method: " + fetchMethod +"\ninput data type: " +dataType);
    }


    public static String getDataFetchType(){return fetchMethod;}

    public static String getSubUrl() {
        return subUrl;
    }

    public static String getPubUrl() {
        return pubUrl;
    }

    public static String getId() {
        return id;
    }

    public static String getDesc() {
        return desc;
    }

    public String getDataType(){
        return dataType;
    }
}
