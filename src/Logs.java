import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;


/**
 * This class is used to create realtime logs which inform the admin about the current status/progress
 * of the application as well as possible errors that may occur.
 */
public class Logs {
    private BufferedWriter br;
    String logsCurrentFileName;

    public Logs(String logsCurrentFileName){
        this.logsCurrentFileName=logsCurrentFileName;
    }

    //Method which creates a LOG, given a specific type.
    //The possible types are 2: ERROR, PROCESS.
    //ERROR:an error or exception has occured.In general,this type of messages will be printed if
    //there is unexpected behaviour of the program.
    public void createLog(String logText,String type){
        try{
            br = new BufferedWriter(new FileWriter(logsCurrentFileName,true));
            if(type.equals("ERROR")){
                String logToPrint = "ERROR " + logText +"\n\r";
                br.append(logToPrint);
                //Files.write(Paths.get(logsCurrentFileName), logToPrint.getBytes(), StandardOpenOption.APPEND);
            }
            else if(type.equals("PROCESS")){
                String logToPrint ="PROCESS " + logText +"\n\r";
                br.append(logToPrint);
                //Files.write(Paths.get(logsCurrentFileName), logToPrint.getBytes(), StandardOpenOption.APPEND);

            }
            br.close();
        }catch(IOException e) {

        }

    }

    //Returns the current date and time which will be used to create the LOG files for each day.
    public static Date getCurrentDate(){
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        java.util.Date date = new java.util.Date();
        Date sqlDate = new Date(date.getTime());
        System.out.println(formatter.format(date));
        return sqlDate;
    }
}
