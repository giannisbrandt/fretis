import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * This class implements a post request to the given web service.
 * The data that have been published over the AEON API,are sent to the below given web service and then they are manipulated accordingly.
 */
public class WebServicePost {
    private Logs programLogs;

    public WebServicePost(Logs programLogs){
        this.programLogs=programLogs;
    }

    public String postData(String toPostString) throws ProtocolException {

        String x = toPostString;

        int length = x.length();
        System.out.println("LENGTH" + length);
        String loadListID = "J1100166689";

        //the request that will be used.
        String request=
                "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                        "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                        "  <soap:Body>\n" +
                        "    <InsertOrders xmlns=\"http://POD_WS.org/KN\">\n" +
                        "      <XmlOrders>"+"<![CDATA["+x+"]]>" +"</XmlOrders>\n" +
                        "      <stringLength>"+x.length()+"</stringLength>\n" +
                        "      <LoadListId>"+loadListID+"</LoadListId>\n" +
                        "    </InsertOrders>\n" +
                        "  </soap:Body>\n" +
                        "</soap:Envelope>";

        String addr = "http://62.103.95.53/POD_WEB/POD_WS/Service.asmx";

        URL url = null;
        try {
            url = new URL(addr);
        } catch (MalformedURLException e1) {
            // TODO Auto-generated catch block
            programLogs.createLog(MainOperation.getCurrentDate() + " Malformed URL exception: " +
            e1.getMessage(),"ERROR");
            e1.printStackTrace();
        }
        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) url.openConnection();
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            programLogs.createLog(MainOperation.getCurrentDate() + " IOException: " +
                    e1.getMessage(),"ERROR");
            e1.printStackTrace();
        }
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Length", String.valueOf(request.getBytes().length));
        connection.setRequestProperty("Content-Type", "text/xml;charset=utf-8;");
        connection.setRequestProperty("Connection", "Keep-Alive");
        connection.setRequestProperty("Accept", "*/*");
        //connection.setRequestProperty("SoapAction", "http://ws.cdyne.com/VerifyEmail");
        connection.setRequestProperty("soapaction", "http://POD_WS.org/KN/InsertOrders");

        connection.setDoOutput(true);
        PrintWriter pw = null;
        try {
            pw = new PrintWriter(connection.getOutputStream());
        } catch (IOException e2) {
            // TODO Auto-generated catch block
            programLogs.createLog(MainOperation.getCurrentDate() + " IOException: " +
                    e2.getMessage(),"ERROR");
            e2.printStackTrace();
        }
        pw.write(request);
        pw.flush();

        try {
            connection.connect();
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            programLogs.createLog(MainOperation.getCurrentDate() + " IOException: " +
                    e1.getMessage(),"ERROR");
            e1.printStackTrace();
        }
        BufferedReader rd = null;
        try {
            rd = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            programLogs.createLog(MainOperation.getCurrentDate() + " IOException: " +
                    e1.getMessage(),"ERROR");
            e1.printStackTrace();
        }
        String line;
        String respond = "";
        try {
            if (rd!=null) {
                respond = rd.readLine();

                while ((line = rd.readLine()) != null)
                    respond = line;
            }
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            programLogs.createLog(MainOperation.getCurrentDate() + " IOException: " +
                    e1.getMessage(),"ERROR");
            e1.printStackTrace();
        }
        return respond;
    }
}
