import net.atos.aeon.AEONInterface;
import net.atos.aeon.AEONSDK;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

/**
 * This class is used to publish data using the AEON realtime communications API,as specified
 * in the documentation.
 */
public class SendData {

    public SendData(JSONObject jsonObject,Logs programLogs) {
        try {
            publishData(jsonObject);
        } catch (MalformedURLException e) {
            programLogs.createLog(e.getMessage().toString() + "\n\r","ERROR");
            e.printStackTrace();
        } catch (JSONException e) {
            programLogs.createLog(e.getMessage().toString() + "\n\r","ERROR");
            e.printStackTrace();
        }
    }

    public static class MyAEONCallbacks implements AEONInterface {
        @Override
        public void deliveredMessage(JSONObject data) {
            System.out.println("Event received" + data.toString());
        }

        @Override
        public void control(JSONObject data) {
            System.out.println("Control Message" + data.toString());
        }

    }

    /**
     *
     * @throws MalformedURLException
     */
    public static void publishData(JSONObject jsonObject) throws MalformedURLException, JSONException {

        AEONSDK sdk = new AEONSDK(Config.PUB_URL,Config.getId(),Config.getDesc());
        try {
            sdk.publish(jsonObject);
            TimeUnit.MICROSECONDS.sleep(200);
            System.out.println("data send");

        }catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
}
