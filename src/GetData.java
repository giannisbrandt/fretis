import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * This method is used to read the appropriate files (CSV or XML) from the folders CSVfiles or
 * XMLfiles where the input files, according to their data structure should be placed respectively.
 *
 */
public class GetData {
    private static ArrayList<String> toReturn;
    private static Logs programLogs;
    private static ArrayList<String> readFiles;


    public GetData(Logs programLogs) {
        this.programLogs = programLogs;
    }

    public static ArrayList<String> getData(int param) {
        toReturn = new ArrayList<>();
        readFiles = new ArrayList<>(); //the files that have been already read by the program are
        //stored here in the listAllFiles method.

        if (param == 1) {                                          //case input data are of XML type.
            //get the path of the folder XMLfiles
            Path csvPath = Paths.get("XMLfiles");
            File file = new File(csvPath.toString());
            toReturn = listAllFiles(file, toReturn, param);
            return toReturn;
        }
        if (param == 2) {                                           //case input data are of CSV type.
            //get the path of the folder CSVfiles
            Path csvPath = Paths.get("CSVfiles");
            File file = new File(csvPath.toString());
            toReturn = listAllFiles(file, toReturn, param);
            return toReturn;
        }


        return toReturn;
    }


    //This method lists all files inside a folder and returns the content of each file which
    // is located in the folder.
    public static ArrayList<String> listAllFiles(File folder, ArrayList<String> toReturn, int param) {
        File[] fileNames = folder.listFiles();
        toReturn.clear();
        for (File file : fileNames) {
            if (!file.isDirectory()) {
                try {
                    BufferedReader br = null;
                    if (param == 1) {
                        br = new BufferedReader(new FileReader(file));
                    } else br = new BufferedReader(new FileReader(file));
                    String line = "";
                    String temp = "";
                    int i = 0;
                    FileCredentials fileCredentials = new FileCredentials(file.getName(), file.toString());

                    while ((line = br.readLine()) != null) {
                        temp += line;
                    }

                    readFiles.add(file.getName());

                    fileCredentials.setContent(temp);
                    toReturn.add(i, temp);
                    i++;
                    br.close();
                } catch (IOException e) {
                    programLogs.createLog("Exception on reading file: " + file.getPath(), "ERROR");
                    GUI.statusUpdating("Exception on reading file: " + file.getPath() + " Exception: " + e.getMessage());
                }
            }
        }
        readFiles.clear();

        //move the files into the appropriate folder (sent or rejected)
            for (File file : fileNames) {
                if (!file.isDirectory()) {

                    if (param == 1) {
                        //file.renameTo(new File("XMLfiles/sent/"+file.getName()));
                        String path = "XMLfiles/sent/" + file.getName();
                        Path path1 = Paths.get("XMLfiles/sent");

                        //readFiles.remove(file.getName());
                        Path from = Paths.get("..\\XMLfiles\\" + file.getName());
                        Path to = Paths.get("..\\XMLfiles\\sent\\" + file.getName());

                        String pathRel = "XMLfiles/sent/" + file.getName();

                        // Use relative path for Unix systems
                        File f = new File(path);
                        try {
                            file.renameTo(f);
                        } catch (Exception e) {
                            e.printStackTrace();
                            programLogs.createLog("Exception on moving XML file: " + file.getPath() + " Exception: " + e.getMessage() + "\n\r", "ERROR");
                            GUI.statusUpdating("Exception on moving file: " + file.getPath());
                        }

                        file.renameTo(new File(path));
                        GUI.statusUpdating("Moved file: " + file.getPath() + " successfully.");
                        programLogs.createLog("File " + file.getName() + "has been moved to XMLfiles/sent folder successfully\n\r", "PROCESS");

                    } else if (param == 2) {
                        //file.renameTo(new File("CSVfiles/sent/"+file.getName()));
                        String path = "CSVfiles/sent/" + file.getName();
                        // Use relative path for Unix systems
                        Path path1 = Paths.get("CSVfiles/sent");


                        //readFiles.remove(file.getName());
                        Path from = Paths.get("..\\CSVfiles\\" + file.getName());
                        Path to = Paths.get("..\\CSVfiles\\sent\\" + file.getName());

                        String pathRel = "CSVfiles/sent/" + file.getName();

                        // Use relative path for Unix systems
                        File f = new File(path);
                        try {
                            file.renameTo(f);
                        } catch (Exception e) {
                            e.printStackTrace();
                            programLogs.createLog("Exception on moving CSV file: " + file.getPath() + " Exception: " + e.getMessage() + "\n\r", "ERROR");
                        }

                        file.renameTo(new File(path));
                        programLogs.createLog("File " + file.getName() + "has been moved to CSVfiles/sent folder successfully\n\r", "PROCESS");
                    }
                }
            }

        return toReturn;
    }


    /**
     * This method moves a given file to the rejected sub-folder of the parent folder CSVfiles or XMLfiles.
     * @param param
     * @param file
     */
    public void moveToRejected(int param,File file){
        if (param == 1) {
            //file.renameTo(new File("XMLfiles/sent/"+file.getName()));
            String path = "XMLfiles/rejected/" + file.getName();

            Path path1 = Paths.get("XMLfiles/rejected");


            //readFiles.remove(file.getName());
            Path from = Paths.get("..\\XMLfiles\\" + file.getName());
            Path to = Paths.get("..\\XMLfiles\\rejected\\" + file.getName());

            String pathRel = "XMLfiles/rejected/" + file.getName();

            // Use relative path for Unix systems
            File f = new File(path);
            try {
                file.renameTo(f);
            } catch (Exception e) {
                e.printStackTrace();
                programLogs.createLog("Exception on moving XML file to rejected folder: " + file.getPath() + " Exception: " + e.getMessage() + "\n\r", "ERROR");

            }

            file.renameTo(new File(path));

        } else if (param == 2) {
            //file.renameTo(new File("CSVfiles/sent/"+file.getName()));
            String path = "CSVfiles/rejected/" + file.getName();
            // Use relative path for Unix systems
            Path path1 = Paths.get("CSVfiles/rejected");


            //readFiles.remove(file.getName());
            Path from = Paths.get("..\\CSVfiles\\" + file.getName());
            Path to = Paths.get("..\\CSVfiles\\rejected\\" + file.getName());

            String pathRel = "CSVfiles/rejected/" + file.getName();

            // Use relative path for Unix systems
            File f = new File(path);
            try {
                file.renameTo(f);
            } catch (Exception e) {
                e.printStackTrace();
                programLogs.createLog("Exception on moving CSV file to rejected folder: " + file.getPath() + " Exception: " + e.getMessage() + "\n\r", "ERROR");
            }

            file.renameTo(new File(path));
        }
    }


}
