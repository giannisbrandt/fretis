import org.json.JSONObject;

import java.io.File;
import java.net.ProtocolException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class MainOperation {
    private static Logs programLogs;
    private static WebServicePost servicePost;
    private static int paused=0;
    private static int noVisited=0;
    private static int i=0;
    private static Config parameters = null;
    private static boolean pauseWritingToGuiFlag = false;

    public static void main(String[] args){
        Main main = new Main();
    }

    public MainOperation(){
        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
        ScheduledExecutorService buttonsResponsesListener = Executors.newSingleThreadScheduledExecutor();

        GUI gui = new GUI(1);

        parameters = new Config();
        if (noVisited==0) {
            parameters.initializeVariables();
            newLogFile();
            noVisited++;
        }

        //Thread which reads the folder of the input files.
        executorService.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                try {

                    if (gui.getStartButtonStatus() == true) {

                        paused++; //increasing value so that next time the STOP button is pressed,
                        //the appropriate log will be written.

                        //initialize object to make post request to the web service.
                        servicePost = new WebServicePost(programLogs);
                        //In case the input data are CSV files
                        if (parameters.getDataType().equals("CSV")) {
                            GetData readCsvFilesObject = new GetData(programLogs);
                            ArrayList<String> csvData = readCsvFilesObject.getData(2);

                            //each index of the array list contains the data of a specific file,
                            //which are sent over the AEON API.
                            for (int i = 0; i < csvData.size(); i++) {
                                String xmlData = null;

                                try {
                                    xmlData = ConvertDataTypes.convertCSVtoXML(csvData.get(i));
                                } catch (ProtocolException e) {
                                    programLogs.createLog(getCurrentDate() + " ProtocolException: " + e.getMessage() + "\n\r", "PROCESS");
                                    e.printStackTrace();
                                }

                                String respond = null; //posting XML data to the web service.

                                try {
                                    respond = servicePost.postData(xmlData);
                                } catch (ProtocolException e) {
                                    programLogs.createLog(getCurrentDate() + " ProtocolException: " + e.getMessage() + "\n\r", "PROCESS");
                                    e.printStackTrace();
                                }

                                programLogs.createLog(getCurrentDate() + " Web service respond: " + respond + "\n\r", "PROCESS");
                                //System.out.println("data content: " + csvData.get(i));
                                JSONObject toSend = ConvertDataTypes.convertXMLtoJSON(xmlData);  //data has to be converted to JSONObject because the API accepts only this type
                                SendData publish = new SendData(toSend, programLogs);
                            }
                            programLogs.createLog(getCurrentDate() + "CSV data have been send\n\r", "PROCESS");
                            GUI.statusUpdating("CSV data have been sent.");
                            csvData.clear();  //empty the arraylist, so that data that have been sent will not be sent again
                        }
                        //In case the input data are XML files
                        else if (parameters.getDataType().equals("XML")) {
                            GetData readXmlFilesObject = new GetData(programLogs);
                            ArrayList<String> xmlString = readXmlFilesObject.getData(1);
                            System.out.println("size" + xmlString.size());
                            //each index of the array list contains the data of a specific file,
                            //which are sent over the AEON API.
                            if (xmlString.size() != 0) {
                                for (int i = 0; i < xmlString.size(); i++) {
                                    String respond = null; //posting XML data to the web service.
                                    try {
                                        respond = servicePost.postData(xmlString.get(i));
                                    } catch (ProtocolException e1) {
                                        e1.printStackTrace();
                                        GUI.statusUpdating("Can't post data to web service.Exception: " + e1.getMessage());
                                    }
                                    programLogs.createLog(getCurrentDate() + " Web service respond: " + respond + "\n\r", "PROCESS");
                                    JSONObject toSend = ConvertDataTypes.convertXMLtoJSON(xmlString.get(i));  //data has to be converted to JSONObject because the API accepts only this type
                                    SendData publish = new SendData(toSend, programLogs);
                                }
                                xmlString.clear();
                            }
                        }


                        //Check if the date has changed and ,if yes, start writing the logs for the new day.
                        int currentLogsDay = getCurrentLogsDay();
                        int currentDay = getCurrentDay();

                        //create new log files for different days.Changes file every 24 hours.
                        if (currentDay != currentLogsDay) {
                            newLogFile();
                        }

                        if (pauseWritingToGuiFlag == false) {
                            GUI.statusUpdating("Waiting for new files...");
                            pauseWritingToGuiFlag = true;
                        }

                    } else if (gui.getStopButtonStatus() == true) {
                        //System is paused;
                        //The flow of the program ends up inside this else-if statement.
                        if (paused == 0) {
                            paused--;
                            programLogs.createLog(getCurrentDate() + " System has been paused.", "PROCESS");
                            GUI.statusUpdating("Program stopped");
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }}, 0, 2, TimeUnit.SECONDS);

        }



    //Returns the current day of the current date.
    public static int getCurrentDay(){
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        java.util.Date date = new java.util.Date();
        String[] dateCredentials = formatter.format(date).split("/");
        return Integer.valueOf(dateCredentials[0]);
    }

    //Returns the current date and time.Is being used to trace the time and date that each log has been printed.
    public static String getCurrentDate(){
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        java.util.Date date = new java.util.Date();
        String[] dateCredentials = formatter.format(date).split(" ");
        String dateString = dateCredentials[0] + " " + dateCredentials[1];
        return dateString;
    }

    //Return day,date,month which is used to create the log files.
    public static String getCurrentLogsDate(){
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        java.util.Date date = new java.util.Date();
        String[] dateCredentials = formatter.format(date).split(" ");
        String[] temp = dateCredentials[0].split("/");
        return temp[0] + "-" + temp[1] + "-" + temp[2];
    }

    //This method returns the current day that is used from the log-writer operations
    //E.g. in which file the program is currently writing the logs
    //(keep in mind that the log files are named after the curent date that they were written)
    private static int getCurrentLogsDay(){
        File previousModifiedFile = getLatestFilefromDir("logs/");
        String nameOfpreviousModifiedFile = previousModifiedFile.getName();
        String[] currentLogsDate = nameOfpreviousModifiedFile.split("-");
        String[] tempToStoreCurrentDay = currentLogsDate[2].split("\\.");
        int currentLogsDay = Integer.valueOf(tempToStoreCurrentDay[0]);
        return Integer.valueOf(currentLogsDay);
    }

    //This method returns the last modified file, given a specific directory.
    private static File getLatestFilefromDir(String dirPath){
        File dir = new File(dirPath);
        File[] files = dir.listFiles();
        if (files == null || files.length == 0) {
            return null;
        }

        File lastModifiedFile = files[0];
        for (int i = 1; i < files.length; i++) {
            if (lastModifiedFile.lastModified() < files[i].lastModified()) {
                lastModifiedFile = files[i];
            }
        }
        return lastModifiedFile;
    }

    //This method creates a new logs file inside the logs directory.
    //The name of the file is the current date.
    public static void newLogFile(){
        try {

            String path = "logs/"+getCurrentLogsDate()+".txt";

            // Use relative path for Unix systems
            File f = new File(path);
            f.createNewFile();
           // f.getParentFile().mkdirs();
           // f.createNewFile();
            programLogs=new Logs(f.getPath());
            //programLogs.createLog("Program started on " + getCurrentDate().toString()  + "\n\r","PROCESS");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
