import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * This class is used to perform operations such as:
 * 1)Transform XML data to CSV data.
 * 2)Transform XML data to JSONObject.
 *
 *  1->Post request to the given web service which returns the transformed data.
 *  2->Pure java,the transformation does not keep the order of the XML file.
 */
public class ConvertDataTypes {
    private static Logs programLogs;


    public ConvertDataTypes(Logs programLogs){
        this.programLogs=programLogs;
    }

    //This method converts XML data to JSONObject type.
    //It is neccessary because the publish() method accepts only objects of the type JSONObject.
    public static JSONObject convertXMLtoJSON(String xmlString){
        JSONObject result = null;

        try{
            result = XML.toJSONObject(xmlString);
        }catch(JSONException e){
            programLogs.createLog("Failed to convert XML data to JSONObject type."+
            "Check if the XML file is on the right format.","ERROR");
            e.printStackTrace();
        }
        return result;
    }


    //post request to web service which converts the CSV data to XML data
    public static String convertCSVtoXML(String csvData) throws ProtocolException {
        String request ="<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                "  <soap:Body>\n" +
                "    <csv2podXML1 xmlns=\"http://tempuri.org/\">\n" +
                "      <csv>"+"<![CDATA["+ csvData +"]]>"+ "</csv>\n" +
                "      <Credentials>"+"CREDENTIALS"+"</Credentials>\n" +
                "    </csv2podXML1>\n" +
                "  </soap:Body>\n" +
                "</soap:Envelope>";

        String addr = "http://62.103.95.53//MappingWS/Service.asmx";

        //trivial implementation of post request to the given web service.
        URL url = null;
        try {
            url = new URL(addr);
        } catch (MalformedURLException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) url.openConnection();
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Length", String.valueOf(request.getBytes().length));
        connection.setRequestProperty("Content-Type", "text/xml;charset=utf-8;");
        connection.setRequestProperty("Connection", "Keep-Alive");
        connection.setRequestProperty("Accept", "*/*");
        //connection.setRequestProperty("SoapAction", "http://ws.cdyne.com/VerifyEmail");
        connection.setRequestProperty("soapaction", "http://tempuri.org/csv2podXML1");

        connection.setDoOutput(true);
        PrintWriter pw = null;
        try {
            pw = new PrintWriter(connection.getOutputStream());
        } catch (IOException e2) {
            // TODO Auto-generated catch block
            e2.printStackTrace();
        }
        pw.write(request);
        System.out.println("REQUEST:"+request);
        pw.flush();

        try {
            connection.connect();
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        BufferedReader rd = null;
        try {
            rd = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        String line;
        String respond = "";
        try {
            if (rd!=null) {
                respond = rd.readLine();

                while ((line = rd.readLine()) != null)
                    respond = line;
            }
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        System.out.println("Respond");
        return respond;
    }
}
