import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * This class implements the visual implementation of the application.
 * When the GUI of the app closes,the application stops running.
 *
 */
public class GUI extends JFrame{
    private static JLabel updateStatus;
    private static boolean startButtonStatus,stopButtonStatus;
    private ScheduledExecutorService startButtonListener;
    private JScrollPane scrollMessages;
    private static boolean pauseWritingFlag=false;

    public GUI(){

        //Initializing GUI:
        JFrame guiFrame = new JFrame("Tredit S.A.");
        guiFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        guiFrame.setSize(520,450);

        Container cont = getContentPane();

        JPanel overviewPanel = new JPanel(new BorderLayout());

        JPanel pane = new JPanel(new BorderLayout());
        //updateStatusLabel && updateStatusLabel2 are showing messages in the top.
        JLabel  updateStatusLabel = new JLabel("Here you can see part of the responses of the running app.");
        Border border0 = updateStatusLabel.getBorder();
        Border margin0 = new EmptyBorder(10,20,10,10);
        updateStatusLabel.setBorder(new CompoundBorder(border0, margin0));

        JLabel  updateStatusLabel2 = new JLabel("For full responses about the program's progress check the log files.");
        Border border2 = updateStatusLabel2.getBorder();
        Border margin2 = new EmptyBorder(10,20,10,10);
        updateStatusLabel2.setBorder(new CompoundBorder(border2, margin2));

        //Setting middle panel messages
        JPanel middlePanel = new JPanel(new GridLayout(2,1));

        JLabel  updateStatusLabel1 = new JLabel("Progress:");
        Border border1 = updateStatusLabel1.getBorder();
        Border margin1 = new EmptyBorder(10,20,10,10);
        updateStatusLabel1.setBorder(new CompoundBorder(border1, margin1));

        //the log messages will appear on this JLabel
        updateStatus = new JLabel();
        Border border = updateStatus.getBorder();
        Border margin = new EmptyBorder(0,60,10,10);
        updateStatus.setBorder(new CompoundBorder(border, margin));
        updateStatus.setText("Program stopped.");

        //Bottom panel BUTTONS to start/stop the application.
        JPanel paneBottom = new JPanel();
        paneBottom.setBounds(10,10,10,10);

        JButton startButton = new JButton("START");
        startButton.setSize(5,5);
        JButton stopButton = new JButton("STOP");
        stopButton.setSize(5,5);

        scrollMessages = new JScrollPane(updateStatus, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

//Implementing action listeners for buttons START and STOP.

        //start button clicked.
        startButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                updateStatus.setText("Program started");
                startButtonStatus=true;
                stopButtonStatus=false;
                //start MainOperation class
                startButtonListener = Executors.newSingleThreadScheduledExecutor();
                startButtonListener.scheduleAtFixedRate(new Runnable() {
                    @Override
                    public void run() {

                        if (stopButtonStatus==false) {
                            new MainOperation();
                        }

                        //stop button has been clicked.
                        stopButton.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                updateStatus.setText("Program stopped");
                                stopButtonStatus=true;
                                startButtonStatus=false;
                            }
                        });

                        if(stopButtonStatus==true){
                            //Program has been stopped.Wait until it starts again.
                            updateStatus.setText("Program stopped");
                        }
                    }
                },0,1, TimeUnit.SECONDS);
            }
        });


        //Making GUI visible.
        pane.add(updateStatusLabel,BorderLayout.NORTH);
        pane.add(updateStatusLabel2,BorderLayout.CENTER);
        overviewPanel.add(pane,BorderLayout.NORTH);

        middlePanel.add(updateStatusLabel1,BorderLayout.NORTH);
        //middlePanel.add(updateStatus,BorderLayout.CENTER);
        middlePanel.add(scrollMessages,BorderLayout.CENTER);
        overviewPanel.add(middlePanel);

        paneBottom.add(startButton);
        paneBottom.add(stopButton);
        overviewPanel.add(paneBottom,BorderLayout.SOUTH);

        guiFrame.add(overviewPanel);
        guiFrame.setVisible(true);



    }


    public GUI(int x){
        //constructor to allow initializing of GUI class without re-establishing the visual represantation.
    }


    //trivial method which updates the text of the JLabel
    public static void statusUpdating(String text){
        if(text.equals("Waiting for new files...") && pauseWritingFlag==false){
            updateStatus.setText("<html>" + updateStatus.getText() + "<br/>" + text );
            pauseWritingFlag=true;
            return;
        }
        else if(!text.equals("Waiting for new files...")) pauseWritingFlag=true;

        updateStatus.setText("<html>" + updateStatus.getText() + "<br/>" + text );
    }

    public boolean getStartButtonStatus(){
        return startButtonStatus;
    }

    public boolean getStopButtonStatus(){
        return stopButtonStatus;
    }
}
