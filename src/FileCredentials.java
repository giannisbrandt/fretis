public class FileCredentials {

    private String fileName,content,filePath;


    public FileCredentials(String fileName,String filePath){
        this.fileName = fileName;
        this.filePath = filePath;
    }

    public FileCredentials(String fileName,String filePath,String content){
        this.fileName = fileName;
        this.filePath = filePath;
        this.content = content;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}
